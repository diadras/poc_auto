﻿using POC_Auto.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace POC_Auto
{
    public class CarService
    {
        private static List<Car> Cars = new List<Car>();
        private static List<Engine> Engines = new List<Engine>();
        private Random random = new Random();
        public Car GetCar(int Id)
        {
            /*foreach (Car car in Cars)
            {
                if(car.CarID == Id)
                {
                    Car = car;
                    break;
                }
            }*/
            return Cars[Id];
        }
        public Engine GetEngine(int Id)
        {
            /*foreach (Engine engine in Engines)
            {
                if(engine.EngID == Id)
                {
                    Engine = engine;
                    break;
                }
            }*/
            return Engines[Id];
        }
        public void AddCar(string brand, Car.CarType type, int weight, double length, double width, Engine engine)
        {
            Car newCar = new Car()
            {
                Brand = brand,
                Type = type,
                Weight = weight,
                Length = length,
                Width = width,
                Engine = engine
            };
            Cars.Add(newCar);
        }
        public List<Car> GetCars()
        {
            return Cars;
        }
        public List<Engine> GetEngines()
        {
            return Engines;
        }
        public void GenerateCars(int Amount = 10)
        {
            for (int i = 0; i < 10; i++)
            {
                //Geef bij 3 niet alle parameters mee om een andere constructor aan te roepen
                Engine engine = new Engine();
                if (i == 1 || i == 4)
                {
                    engine.Name = "Not overloaded Engine";
                }
                else
                { 
                    engine.Cylinders = GetRandomInt(2, 12, true);
                    engine.HP = GetRandomInt(100, 500);
                    engine.Name = RandomString(1, false) + RandomString(6, true);
                }
                Engines.Add(engine);

                Car car = new Car();
                car.Engine = engine;
                car.Type = (Car.CarType)GetRandomInt(2, 9);
                car.Weight = GetRandomInt(700, 1500);
                car.Brand = RandomString(1, false) + RandomString(6, true);
                car.Length = Math.Round(GetRandomDouble(150, 300), 2);
                car.Width = Math.Round(GetRandomDouble(100, 200), 2);

                Cars.Add(car);
            }
        }

/*_________________________Random generators__________________________*/
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        public double GetRandomDouble(double minimum, double maximum)
        {
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
        public int GetRandomInt(int minimum, int maximum, bool modulo = false)
        {
            int generatedInt = random.Next(minimum, maximum);
            if (modulo) {
                //Doe het volgende wanneer generatedInt NIET gelijk is aan 3 en generatedInt NIET even is of generatedInt gelijk is aan 2
                while ((generatedInt != 3) && (generatedInt % 2 != 0) || (generatedInt == 2))
                {
                    generatedInt = random.Next(minimum, maximum);
                }
            }
            return generatedInt;
        }
/*_________________________Random generators___________________*/
    }
}