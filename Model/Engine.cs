﻿using System.Collections.Generic;

namespace POC_Auto.Model
{
    public class Engine
    {
        private static int NumEng = 0;
        public int EngID { get; }
        public string Name { get; set; }
        public int HP { get; set; }
        public int Cylinders { get; set; }
        public Engine()
        {
            NumEng++;
            EngID = NumEng;
            Name = "Generic Engine";
            HP = 150;
            Cylinders = 3;
        }
        public Engine(string name, int hp, int cylinders)
        {
            NumEng++;
            EngID = NumEng;
            Name = name;
            HP = hp;
            Cylinders = cylinders;
        }
    }
}