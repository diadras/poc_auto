﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC_Auto.Model
{
    public class Car
    {
        private static int NumCars = 0;
        public int CarID { get; }
        public string Brand { get; set; }
        public CarType Type { get; set; }
        public int Weight { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public Engine Engine { get; set; }

        public Car()
        {
            NumCars++;
            CarID = NumCars;
        }
        /*public Car(string brand, Car.CarType type, int weight, double length, double width, Engine engine)
        {
            //Shorthand if-statements werken niet in een constructor?
            //brand == "" ? Brand = "Good Car" : Brand = brand;
            if(brand == "") { Brand = "Good Car"; } else { Brand = brand; };

            //type == (CarType)1 ? Type = (CarType)5 : Type = type;
            if (type == (CarType)1) { Type = (CarType)5; } else { Type = type; };

            if (weight == 0) { Weight = 1000; } else { Weight = weight; };
            if (length == 0) { Length = 199.95; } else { Length = length; };
            if (width == 0) { Width = 149.95; } else { Width = width; };
        }*/
        public enum CarType
        {
            None = 1,
            Coupe = 2,
            Saloon = 3,
            Hatchback = 4,
            Sedan = 5,
            Sport = 6,
            Jeep = 7,
            EstateCar = 8,
            StationWagon = 9
        }
        public Array GenerateEnums()
        {
            return Enum.GetValues(typeof(CarType));
        }
        public double GetSpeed()
        {
            return this.Weight + this.Length + this.Width + this.Engine.HP;
        }
        public double GetSpeed(int Weight, double Length, double Width, int Hp)
        {
            return Weight + Length + Width + Hp;
        }
    }
}
