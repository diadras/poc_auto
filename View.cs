﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using POC_Auto.Model;

namespace POC_Auto
{
    public partial class View : Form
    {
        public View()
        {
            InitializeComponent();
            HPNumeric.Value = 90;
            TypeComboBox.DataSource = Enum.GetValues(typeof(Car.CarType));
        }
        private void FillCarByID(int ID)
        {
            Car result = Controller.carService.GetCar(ID);
            IDShowLabel.Text = result.CarID.ToString();
            BrandTextBox.Text = result.Brand;
            TypeComboBox.SelectedIndex = (int)result.Type - 1;
            WeightNumeric.Text = result.Weight.ToString();
            LengthNumeric.Text = result.Length.ToString();
            WidthNumeric.Text = result.Width.ToString();
            EngineComboBox.SelectedIndex = result.Engine.EngID-1;
            FillEngineByID(result.Engine.EngID);
            FillTopSpeedByCar(result);
        }
        private void FillTopSpeedByCar(Car car)
        {
            TopSpeedLabel.Text = car.GetSpeed().ToString();
        }
        private void FillEngineByID(int ID)
        {
            Engine result = Controller.carService.GetEngine(ID-1);
            EngineIDLabel.Text = result.EngID.ToString();
            EngineNameTextBox.Text = result.Name;
            HPNumeric.Text = result.HP.ToString();
            CylinderNumeric.Text = result.Cylinders.ToString();
        }
        private void GenCarsButton_Click(object sender, EventArgs e)
        {
            Controller.carService.GenerateCars();
        }
        private void ListCarsButton_Click(object sender, EventArgs e)
        {
            foreach(string carString in Controller.ListCars())
            {
                ViewListBox.Items.Add(carString);
            }
            Controller.ListCars();

            EngineComboBox.DataSource = new BindingSource(Controller.ListEngines(), null);
            EngineComboBox.DisplayMember = "Value";
            EngineComboBox.ValueMember = "Key";
            
        }
        private void SearchByIDButton_Click(object sender, EventArgs e)
        {
            int ID = Convert.ToInt32(IDSearchNumeric.Text)-1;
            ViewListBox.Items.Clear();
            foreach (string carString in Controller.ListCarByID(ID))
            {
                ViewListBox.Items.Add(carString);
            }    
            FillCarByID(ID);
            
        }
        private void CylinderNumeric_ValueChanged(object sender, EventArgs e)
        {
            int CylinderVal = Convert.ToInt32(CylinderNumeric.Value);
            if ((CylinderVal != 3) && (CylinderVal % 2 != 0) || (CylinderVal == 2))
            {
                CylinderNumeric.Value = CylinderVal + 1;
            }
        }
        private void HPNumeric_ValueChanged(object sender, EventArgs e)
        {
            int HPVal = Convert.ToInt32(HPNumeric.Value);
            if (HPVal < 90)
            {
                HPNumeric.Value = 90;
            }
        }
        private void EngineComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
                FillEngineByID(EngineComboBox.SelectedIndex +1);
        }

        private void AddCarButton_Click(object sender, EventArgs e)
        {
            string brand = BrandTextBox.Text;
            //Pak de selected index, doe die +1, pak van die value de enum uit CarType 
            //en geef die mee naar "type
            Car.CarType type = (Car.CarType)TypeComboBox.SelectedIndex+1;
            int weight = Convert.ToInt32(WeightNumeric.Text);
            double length = Convert.ToDouble(LengthNumeric.Text);
            double width = Convert.ToDouble(WidthNumeric.Text);
            Engine engine = Controller.carService.GetEngine(EngineComboBox.SelectedIndex);

            Controller.carService.AddCar(brand,type,weight,length,width,engine);
        }
    }
}
