﻿namespace POC_Auto
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataSet = new POC_Auto.DataSet();
            this.dataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.carsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.carsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.ViewListBox = new System.Windows.Forms.ListBox();
            this.GenCarsButton = new System.Windows.Forms.Button();
            this.ListCarsButton = new System.Windows.Forms.Button();
            this.IDSearchLabel = new System.Windows.Forms.Label();
            this.SearchByIDButton = new System.Windows.Forms.Button();
            this.BrandTextBox = new System.Windows.Forms.TextBox();
            this.AddEditCarLabel = new System.Windows.Forms.Label();
            this.IDEditLabel = new System.Windows.Forms.Label();
            this.BrandLabel = new System.Windows.Forms.Label();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.WeightLabel = new System.Windows.Forms.Label();
            this.LengthLabel = new System.Windows.Forms.Label();
            this.WidthLabel = new System.Windows.Forms.Label();
            this.TypeComboBox = new System.Windows.Forms.ComboBox();
            this.AddEditEngineLabel = new System.Windows.Forms.Label();
            this.CylinderLabel = new System.Windows.Forms.Label();
            this.HPTextBox = new System.Windows.Forms.Label();
            this.HPNumeric = new System.Windows.Forms.NumericUpDown();
            this.CylinderNumeric = new System.Windows.Forms.NumericUpDown();
            this.WeightNumeric = new System.Windows.Forms.NumericUpDown();
            this.LengthNumeric = new System.Windows.Forms.NumericUpDown();
            this.WidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.IDShowLabel = new System.Windows.Forms.Label();
            this.IDSearchNumeric = new System.Windows.Forms.NumericUpDown();
            this.EngineNameLabel = new System.Windows.Forms.Label();
            this.EngineNameTextBox = new System.Windows.Forms.TextBox();
            this.EngineLabel = new System.Windows.Forms.Label();
            this.EngineComboBox = new System.Windows.Forms.ComboBox();
            this.EngineIDLabel = new System.Windows.Forms.Label();
            this.EngineIDLeftLabel = new System.Windows.Forms.Label();
            this.TopSpeedShowLabel = new System.Windows.Forms.Label();
            this.TopSpeedLabel = new System.Windows.Forms.Label();
            this.AddCarButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CylinderNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeightNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDSearchNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSet
            // 
            this.dataSet.DataSetName = "DataSet";
            this.dataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSetBindingSource
            // 
            this.dataSetBindingSource.DataSource = this.dataSet;
            this.dataSetBindingSource.Position = 0;
            // 
            // carsBindingSource
            // 
            this.carsBindingSource.DataMember = "Cars";
            this.carsBindingSource.DataSource = this.dataSetBindingSource;
            // 
            // carsBindingSource1
            // 
            this.carsBindingSource1.DataMember = "Cars";
            this.carsBindingSource1.DataSource = this.dataSetBindingSource;
            // 
            // ViewListBox
            // 
            this.ViewListBox.FormattingEnabled = true;
            this.ViewListBox.Location = new System.Drawing.Point(394, 12);
            this.ViewListBox.Name = "ViewListBox";
            this.ViewListBox.Size = new System.Drawing.Size(404, 524);
            this.ViewListBox.TabIndex = 1;
            // 
            // GenCarsButton
            // 
            this.GenCarsButton.Location = new System.Drawing.Point(804, 12);
            this.GenCarsButton.Name = "GenCarsButton";
            this.GenCarsButton.Size = new System.Drawing.Size(128, 36);
            this.GenCarsButton.TabIndex = 2;
            this.GenCarsButton.Text = "Generate Cars";
            this.GenCarsButton.UseVisualStyleBackColor = true;
            this.GenCarsButton.Click += new System.EventHandler(this.GenCarsButton_Click);
            // 
            // ListCarsButton
            // 
            this.ListCarsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ListCarsButton.Location = new System.Drawing.Point(804, 54);
            this.ListCarsButton.Name = "ListCarsButton";
            this.ListCarsButton.Size = new System.Drawing.Size(128, 39);
            this.ListCarsButton.TabIndex = 3;
            this.ListCarsButton.Text = "List Cars";
            this.ListCarsButton.UseVisualStyleBackColor = true;
            this.ListCarsButton.Click += new System.EventHandler(this.ListCarsButton_Click);
            // 
            // IDSearchLabel
            // 
            this.IDSearchLabel.AutoSize = true;
            this.IDSearchLabel.Location = new System.Drawing.Point(810, 102);
            this.IDSearchLabel.Name = "IDSearchLabel";
            this.IDSearchLabel.Size = new System.Drawing.Size(18, 13);
            this.IDSearchLabel.TabIndex = 4;
            this.IDSearchLabel.Text = "ID";
            // 
            // SearchByIDButton
            // 
            this.SearchByIDButton.Location = new System.Drawing.Point(808, 125);
            this.SearchByIDButton.Name = "SearchByIDButton";
            this.SearchByIDButton.Size = new System.Drawing.Size(124, 28);
            this.SearchByIDButton.TabIndex = 6;
            this.SearchByIDButton.Text = "Search by ID";
            this.SearchByIDButton.UseVisualStyleBackColor = true;
            this.SearchByIDButton.Click += new System.EventHandler(this.SearchByIDButton_Click);
            // 
            // BrandTextBox
            // 
            this.BrandTextBox.Location = new System.Drawing.Point(111, 90);
            this.BrandTextBox.Name = "BrandTextBox";
            this.BrandTextBox.Size = new System.Drawing.Size(100, 20);
            this.BrandTextBox.TabIndex = 8;
            // 
            // AddEditCarLabel
            // 
            this.AddEditCarLabel.AutoSize = true;
            this.AddEditCarLabel.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddEditCarLabel.Location = new System.Drawing.Point(13, 13);
            this.AddEditCarLabel.Name = "AddEditCarLabel";
            this.AddEditCarLabel.Size = new System.Drawing.Size(189, 40);
            this.AddEditCarLabel.TabIndex = 9;
            this.AddEditCarLabel.Text = "Add/edit car";
            // 
            // IDEditLabel
            // 
            this.IDEditLabel.AutoSize = true;
            this.IDEditLabel.Location = new System.Drawing.Point(37, 67);
            this.IDEditLabel.Name = "IDEditLabel";
            this.IDEditLabel.Size = new System.Drawing.Size(18, 13);
            this.IDEditLabel.TabIndex = 14;
            this.IDEditLabel.Text = "ID";
            // 
            // BrandLabel
            // 
            this.BrandLabel.AutoSize = true;
            this.BrandLabel.Location = new System.Drawing.Point(37, 93);
            this.BrandLabel.Name = "BrandLabel";
            this.BrandLabel.Size = new System.Drawing.Size(35, 13);
            this.BrandLabel.TabIndex = 15;
            this.BrandLabel.Text = "Brand";
            // 
            // TypeLabel
            // 
            this.TypeLabel.AutoSize = true;
            this.TypeLabel.Location = new System.Drawing.Point(37, 119);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(31, 13);
            this.TypeLabel.TabIndex = 16;
            this.TypeLabel.Text = "Type";
            // 
            // WeightLabel
            // 
            this.WeightLabel.AutoSize = true;
            this.WeightLabel.Location = new System.Drawing.Point(37, 145);
            this.WeightLabel.Name = "WeightLabel";
            this.WeightLabel.Size = new System.Drawing.Size(41, 13);
            this.WeightLabel.TabIndex = 17;
            this.WeightLabel.Text = "Weight";
            // 
            // LengthLabel
            // 
            this.LengthLabel.AutoSize = true;
            this.LengthLabel.Location = new System.Drawing.Point(37, 171);
            this.LengthLabel.Name = "LengthLabel";
            this.LengthLabel.Size = new System.Drawing.Size(40, 13);
            this.LengthLabel.TabIndex = 18;
            this.LengthLabel.Text = "Length";
            // 
            // WidthLabel
            // 
            this.WidthLabel.AutoSize = true;
            this.WidthLabel.Location = new System.Drawing.Point(37, 197);
            this.WidthLabel.Name = "WidthLabel";
            this.WidthLabel.Size = new System.Drawing.Size(35, 13);
            this.WidthLabel.TabIndex = 19;
            this.WidthLabel.Text = "Width";
            // 
            // TypeComboBox
            // 
            this.TypeComboBox.FormattingEnabled = true;
            this.TypeComboBox.Location = new System.Drawing.Point(111, 117);
            this.TypeComboBox.Name = "TypeComboBox";
            this.TypeComboBox.Size = new System.Drawing.Size(100, 21);
            this.TypeComboBox.TabIndex = 21;
            // 
            // AddEditEngineLabel
            // 
            this.AddEditEngineLabel.AutoSize = true;
            this.AddEditEngineLabel.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddEditEngineLabel.Location = new System.Drawing.Point(13, 307);
            this.AddEditEngineLabel.Name = "AddEditEngineLabel";
            this.AddEditEngineLabel.Size = new System.Drawing.Size(240, 40);
            this.AddEditEngineLabel.TabIndex = 23;
            this.AddEditEngineLabel.Text = "Add/edit Engine";
            // 
            // CylinderLabel
            // 
            this.CylinderLabel.AutoSize = true;
            this.CylinderLabel.Location = new System.Drawing.Point(37, 442);
            this.CylinderLabel.Name = "CylinderLabel";
            this.CylinderLabel.Size = new System.Drawing.Size(49, 13);
            this.CylinderLabel.TabIndex = 27;
            this.CylinderLabel.Text = "Cylinders";
            // 
            // HPTextBox
            // 
            this.HPTextBox.AutoSize = true;
            this.HPTextBox.Location = new System.Drawing.Point(37, 416);
            this.HPTextBox.Name = "HPTextBox";
            this.HPTextBox.Size = new System.Drawing.Size(65, 13);
            this.HPTextBox.TabIndex = 26;
            this.HPTextBox.Text = "HorsePower";
            // 
            // HPNumeric
            // 
            this.HPNumeric.Location = new System.Drawing.Point(111, 416);
            this.HPNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.HPNumeric.Name = "HPNumeric";
            this.HPNumeric.Size = new System.Drawing.Size(100, 20);
            this.HPNumeric.TabIndex = 28;
            this.HPNumeric.ValueChanged += new System.EventHandler(this.HPNumeric_ValueChanged);
            // 
            // CylinderNumeric
            // 
            this.CylinderNumeric.Location = new System.Drawing.Point(111, 442);
            this.CylinderNumeric.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.CylinderNumeric.Name = "CylinderNumeric";
            this.CylinderNumeric.Size = new System.Drawing.Size(100, 20);
            this.CylinderNumeric.TabIndex = 29;
            this.CylinderNumeric.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.CylinderNumeric.ValueChanged += new System.EventHandler(this.CylinderNumeric_ValueChanged);
            // 
            // WeightNumeric
            // 
            this.WeightNumeric.Location = new System.Drawing.Point(111, 143);
            this.WeightNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.WeightNumeric.Name = "WeightNumeric";
            this.WeightNumeric.Size = new System.Drawing.Size(100, 20);
            this.WeightNumeric.TabIndex = 30;
            // 
            // LengthNumeric
            // 
            this.LengthNumeric.Location = new System.Drawing.Point(111, 169);
            this.LengthNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.LengthNumeric.Name = "LengthNumeric";
            this.LengthNumeric.Size = new System.Drawing.Size(100, 20);
            this.LengthNumeric.TabIndex = 31;
            // 
            // WidthNumeric
            // 
            this.WidthNumeric.Location = new System.Drawing.Point(111, 195);
            this.WidthNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.WidthNumeric.Name = "WidthNumeric";
            this.WidthNumeric.Size = new System.Drawing.Size(100, 20);
            this.WidthNumeric.TabIndex = 32;
            // 
            // IDShowLabel
            // 
            this.IDShowLabel.AutoSize = true;
            this.IDShowLabel.Location = new System.Drawing.Point(108, 67);
            this.IDShowLabel.Name = "IDShowLabel";
            this.IDShowLabel.Size = new System.Drawing.Size(18, 13);
            this.IDShowLabel.TabIndex = 33;
            this.IDShowLabel.Text = "ID";
            // 
            // IDSearchNumeric
            // 
            this.IDSearchNumeric.Location = new System.Drawing.Point(834, 100);
            this.IDSearchNumeric.Name = "IDSearchNumeric";
            this.IDSearchNumeric.Size = new System.Drawing.Size(98, 20);
            this.IDSearchNumeric.TabIndex = 34;
            // 
            // EngineNameLabel
            // 
            this.EngineNameLabel.AutoSize = true;
            this.EngineNameLabel.Location = new System.Drawing.Point(37, 390);
            this.EngineNameLabel.Name = "EngineNameLabel";
            this.EngineNameLabel.Size = new System.Drawing.Size(68, 13);
            this.EngineNameLabel.TabIndex = 35;
            this.EngineNameLabel.Text = "EngineName";
            // 
            // EngineNameTextBox
            // 
            this.EngineNameTextBox.Location = new System.Drawing.Point(111, 387);
            this.EngineNameTextBox.Name = "EngineNameTextBox";
            this.EngineNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.EngineNameTextBox.TabIndex = 36;
            // 
            // EngineLabel
            // 
            this.EngineLabel.AutoSize = true;
            this.EngineLabel.Location = new System.Drawing.Point(37, 223);
            this.EngineLabel.Name = "EngineLabel";
            this.EngineLabel.Size = new System.Drawing.Size(40, 13);
            this.EngineLabel.TabIndex = 22;
            this.EngineLabel.Text = "Engine";
            // 
            // EngineComboBox
            // 
            this.EngineComboBox.FormattingEnabled = true;
            this.EngineComboBox.Location = new System.Drawing.Point(111, 220);
            this.EngineComboBox.Name = "EngineComboBox";
            this.EngineComboBox.Size = new System.Drawing.Size(100, 21);
            this.EngineComboBox.TabIndex = 20;
            this.EngineComboBox.SelectedIndexChanged += new System.EventHandler(this.EngineComboBox_SelectedIndexChanged);
            // 
            // EngineIDLabel
            // 
            this.EngineIDLabel.AutoSize = true;
            this.EngineIDLabel.Location = new System.Drawing.Point(108, 364);
            this.EngineIDLabel.Name = "EngineIDLabel";
            this.EngineIDLabel.Size = new System.Drawing.Size(18, 13);
            this.EngineIDLabel.TabIndex = 38;
            this.EngineIDLabel.Text = "ID";
            // 
            // EngineIDLeftLabel
            // 
            this.EngineIDLeftLabel.AutoSize = true;
            this.EngineIDLeftLabel.Location = new System.Drawing.Point(37, 364);
            this.EngineIDLeftLabel.Name = "EngineIDLeftLabel";
            this.EngineIDLeftLabel.Size = new System.Drawing.Size(18, 13);
            this.EngineIDLeftLabel.TabIndex = 37;
            this.EngineIDLeftLabel.Text = "ID";
            // 
            // TopSpeedShowLabel
            // 
            this.TopSpeedShowLabel.AutoSize = true;
            this.TopSpeedShowLabel.Location = new System.Drawing.Point(260, 54);
            this.TopSpeedShowLabel.Name = "TopSpeedShowLabel";
            this.TopSpeedShowLabel.Size = new System.Drawing.Size(63, 13);
            this.TopSpeedShowLabel.TabIndex = 39;
            this.TopSpeedShowLabel.Text = "Top Speed:";
            // 
            // TopSpeedLabel
            // 
            this.TopSpeedLabel.AutoSize = true;
            this.TopSpeedLabel.Location = new System.Drawing.Point(329, 54);
            this.TopSpeedLabel.Name = "TopSpeedLabel";
            this.TopSpeedLabel.Size = new System.Drawing.Size(13, 13);
            this.TopSpeedLabel.TabIndex = 40;
            this.TopSpeedLabel.Text = "0";
            // 
            // AddCarButton
            // 
            this.AddCarButton.Location = new System.Drawing.Point(111, 259);
            this.AddCarButton.Name = "AddCarButton";
            this.AddCarButton.Size = new System.Drawing.Size(91, 28);
            this.AddCarButton.TabIndex = 41;
            this.AddCarButton.Text = "Add car";
            this.AddCarButton.UseVisualStyleBackColor = true;
            this.AddCarButton.Click += new System.EventHandler(this.AddCarButton_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 561);
            this.Controls.Add(this.AddCarButton);
            this.Controls.Add(this.TopSpeedLabel);
            this.Controls.Add(this.TopSpeedShowLabel);
            this.Controls.Add(this.EngineIDLabel);
            this.Controls.Add(this.EngineIDLeftLabel);
            this.Controls.Add(this.EngineNameTextBox);
            this.Controls.Add(this.EngineNameLabel);
            this.Controls.Add(this.IDSearchNumeric);
            this.Controls.Add(this.IDShowLabel);
            this.Controls.Add(this.WidthNumeric);
            this.Controls.Add(this.LengthNumeric);
            this.Controls.Add(this.WeightNumeric);
            this.Controls.Add(this.CylinderNumeric);
            this.Controls.Add(this.HPNumeric);
            this.Controls.Add(this.CylinderLabel);
            this.Controls.Add(this.HPTextBox);
            this.Controls.Add(this.AddEditEngineLabel);
            this.Controls.Add(this.EngineLabel);
            this.Controls.Add(this.TypeComboBox);
            this.Controls.Add(this.EngineComboBox);
            this.Controls.Add(this.WidthLabel);
            this.Controls.Add(this.LengthLabel);
            this.Controls.Add(this.WeightLabel);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.BrandLabel);
            this.Controls.Add(this.IDEditLabel);
            this.Controls.Add(this.AddEditCarLabel);
            this.Controls.Add(this.BrandTextBox);
            this.Controls.Add(this.SearchByIDButton);
            this.Controls.Add(this.IDSearchLabel);
            this.Controls.Add(this.ListCarsButton);
            this.Controls.Add(this.GenCarsButton);
            this.Controls.Add(this.ViewListBox);
            this.Name = "View";
            this.Text = "POC Auto simulatie";
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CylinderNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeightNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDSearchNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource dataSetBindingSource;
        private DataSet dataSet;
        private System.Windows.Forms.BindingSource carsBindingSource;
        private System.Windows.Forms.BindingSource carsBindingSource1;
        private System.Windows.Forms.ListBox ViewListBox;
        private System.Windows.Forms.Button GenCarsButton;
        private System.Windows.Forms.Button ListCarsButton;
        private System.Windows.Forms.Label IDSearchLabel;
        private System.Windows.Forms.Button SearchByIDButton;
        private System.Windows.Forms.TextBox BrandTextBox;
        private System.Windows.Forms.Label AddEditCarLabel;
        private System.Windows.Forms.Label IDEditLabel;
        private System.Windows.Forms.Label BrandLabel;
        private System.Windows.Forms.Label TypeLabel;
        private System.Windows.Forms.Label WeightLabel;
        private System.Windows.Forms.Label LengthLabel;
        private System.Windows.Forms.Label WidthLabel;
        private System.Windows.Forms.ComboBox TypeComboBox;
        private System.Windows.Forms.Label AddEditEngineLabel;
        private System.Windows.Forms.Label CylinderLabel;
        private System.Windows.Forms.Label HPTextBox;
        private System.Windows.Forms.NumericUpDown HPNumeric;
        private System.Windows.Forms.NumericUpDown CylinderNumeric;
        private System.Windows.Forms.NumericUpDown WeightNumeric;
        private System.Windows.Forms.NumericUpDown LengthNumeric;
        private System.Windows.Forms.NumericUpDown WidthNumeric;
        private System.Windows.Forms.Label IDShowLabel;
        private System.Windows.Forms.NumericUpDown IDSearchNumeric;
        private System.Windows.Forms.Label EngineNameLabel;
        private System.Windows.Forms.TextBox EngineNameTextBox;
        private System.Windows.Forms.Label EngineLabel;
        private System.Windows.Forms.ComboBox EngineComboBox;
        private System.Windows.Forms.Label EngineIDLabel;
        private System.Windows.Forms.Label EngineIDLeftLabel;
        private System.Windows.Forms.Label TopSpeedShowLabel;
        private System.Windows.Forms.Label TopSpeedLabel;
        private System.Windows.Forms.Button AddCarButton;
    }
}

