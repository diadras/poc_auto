﻿using POC_Auto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POC_Auto
{
    class Controller
    {
        public static CarService carService = new CarService();
        public static Dictionary<int,string> ListEngines()
        {
            Dictionary<int, string> engineList = new Dictionary<int, string>();
            List<Engine> engines = carService.GetEngines();

            foreach (Engine engine in engines)
            {
                engineList.Add(engine.EngID, engine.Name);
            }
            return engineList;
        }
        public static List<string> ListCars()
        {
            List<string> carList = new List<string>();
            carList.Clear();
            List < Car > cars = carService.GetCars();
            int counter = 0;
            //Voor elke car in carService
            foreach (Car car in cars)
            {
                carList.Add($"Car {counter + 1} properties:");
                //Voor alle properties van object Car doe:
                foreach (PropertyInfo prop in car.GetType().GetProperties())
                {
                    //Als de property GEEN Engine is dan:
                    if (prop.Name != "Engine")
                    {
                        //Zet in listBox een nieuwe regel met een tab, de huidige property-naam en de waarde van deze property uit het object Car.
                        carList.Add("\t" + prop.Name + ": " + prop.GetValue(car, null).ToString());
                    }
                    //Als de property WEL Engine is dan:
                    else
                    {
                        carList.Add("Engine properties:");
                        //Voor alle properties van object Engine doe:
                        foreach (PropertyInfo prop2 in car.Engine.GetType().GetProperties())
                        {
                            //als de property GEEN Engines is dan:
                            if (prop2.Name != "Engines")
                            {
                                //Zet in listBox een nieuwe regel met een tab, de huidige property-naam en de waarde van deze property uit het object Engine.
                                carList.Add("\t" + prop2.Name + ": " + prop2.GetValue(car.Engine, null).ToString());
                            }
                        }
                    }
                }
                carList.Add("");
                counter++;
                
            }
            return carList;
        }
        public static List<string> ListCars(bool console)
        {
            List<string> carList = new List<string>();
            carList.Clear();
            List<Car> cars = carService.GetCars();
            int counter = 0;
            //Voor elke car in carService
            foreach (Car car in cars)
            {
                carList.Add($"Car {counter + 1} properties:");
                //Voor alle properties van object Car doe:
                foreach (PropertyInfo prop in car.GetType().GetProperties())
                {
                    //Als de property GEEN Engine is dan:
                    if (prop.Name != "Engine")
                    {
                        //Zet in listBox een nieuwe regel met een tab, de huidige property-naam en de waarde van deze property uit het object Car.
                        if (console != true)
                        {
                            carList.Add("\t" + prop.Name + ": " + prop.GetValue(car, null).ToString());
                        } else
                        {
                            carList.Add(prop.Name + ": " + prop.GetValue(car, null).ToString());
                        }
                    }
                    //Als de property WEL Engine is dan:
                    else
                    {
                        carList.Add("Engine properties:");
                        //Voor alle properties van object Engine doe:
                        foreach (PropertyInfo prop2 in car.Engine.GetType().GetProperties())
                        {
                            //als de property GEEN Engines is dan:
                            if (prop2.Name != "Engines")
                            {
                                if (console != true)
                                {
                                    //Zet in listBox een nieuwe regel met een tab, de huidige property-naam en de waarde van deze property uit het object Engine.
                                    carList.Add("\t" + prop2.Name + ": " + prop2.GetValue(car.Engine, null).ToString());
                                } else
                                {
                                    carList.Add(prop2.Name + ": " + prop2.GetValue(car.Engine, null).ToString());
                                }
                            }
                        }
                    }
                }
                carList.Add("");
                counter++;

            }
            return carList;
        }
        public static List<string> ListCarByID(int ID)
        {
            List<string> carList = new List<string>();
            if (ID != 0)
            {

                carList.Clear();
                Car car = carService.GetCar(ID);

                carList.Add($"Car {ID+1} properties:");
                //Voor alle properties van object Car doe:
                foreach (PropertyInfo prop in car.GetType().GetProperties())
                {
                    //Als de property GEEN Engine is dan:
                    if (prop.Name != "Engine")
                    {
                        //Zet in listBox een nieuwe regel met een tab, de huidige property-naam en de waarde van deze property uit het object Car.
                        carList.Add("\t" + prop.Name + ": " + prop.GetValue(car, null).ToString());
                    }
                    //Als de property WEL Engine is dan:
                    else
                    {
                        carList.Add("Engine properties:");
                        //Voor alle properties van object Engine doe:
                        foreach (PropertyInfo prop2 in car.Engine.GetType().GetProperties())
                        {
                            //als de property GEEN Engines is dan:
                            if (prop2.Name != "Engines")
                            {
                                //Zet in listBox een nieuwe regel met een tab, de huidige property-naam en de waarde van deze property uit het object Engine.
                                carList.Add("\t" + prop2.Name + ": " + prop2.GetValue(car.Engine, null).ToString());
                            }
                        }
                    }
                }
            }
            return carList;
        }
    }
}
