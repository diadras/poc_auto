﻿Dit is een POC om mijn kennis in C# en de leeruitkomsten van de verdiepende fase aan te tonen.
In dit programma kan je autos genereren, selecteren bij ID en zelf autos aanmaken.

Er is te zien dat ik weet hoe ik object georienteerd moet programmeren,
hoe ik de scheiding van GUI en domein goed hanteer en op de juiste manier objecten instantieer of statisch maak.
Verder is te zien dat ik de basisvaardigheden begrijp en gebruik.

Wat wilt de klant?
Ik wil als klant auto’s en hun eigenschappen kunnen invoeren en op die manier de topsnelheid kunnen zien.
Graag zie ik hierbij een foto van de auto die ik heb gekozen met de eigenschappen die erbij horen.
Ook moet ik de auto’s kunnen opslaan, opnieuw kunnen ophalen en aanpassen.

